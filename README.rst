=======================================
libmatrixdb - 2D matrix storage library
=======================================

Dependencies
============

- libm, libc, libz (zlib), libsqlite3 (all of these are likely to be available on a default installation)
- The `Armadillo <http://arma.sourceforge.net/>`_ C++ linear algebra library
- A C++ compiler supporting the C++14 standard
- cmake
- Google spdlog (spdlog-git in Arch Linux)

Installation
============

Example for compiling and installing in **~/.local**:

.. code-block:: bash
    $ git clone https://gitlab.com/wrenlab/libmatrixdb.git
    $ cd libmatrixdb
    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_INSTALL_PREFIX:PATH=~/.local ..
    $ make all install

This will install the CLI **matrixdb** into ~/.local/bin and the shared
library, **libmatrixdb.so** into ~/.local/lib. No matter where you choose to
install, ensure **$DESTINATION/lib** is included your **$LD_LIBRARY_PATH**
environment variable.

Usage
=====

The command-line interface is called **matrixdb**, which is (not yet)
documented with a man page.

The shared library is primarily intended for use with the `Python bindings
<http://gitlab.com/wrenlab/matrixdb>`_ for this package, but is also needed for
the CLI.
