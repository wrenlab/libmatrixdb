========
matrixdb
========

-----------------------------------
CLI for manipulating MatrixDB files
-----------------------------------

:Author: gilesc@omrf.org
:Date:   2017-11-03
:Copyright: AGPLv3+
:Version: 0.1
:Manual section: 1
:Manual group: database

SYNOPSIS
========

matrixdb <subcommand> X.matrixdb <args>

DESCRIPTION
===========

`matrixdb` provides several subcommands for creating and querying matrixdb-format files.
