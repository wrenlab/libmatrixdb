#pragma once

#include <memory>
#include <string>
#include <fstream>
#include <cassert>

#include <matrixdb/types.hpp>

namespace matrixdb {

class SeriesReader {
private:
    bool _eof = false;
    bool strict;
    Series current;
    std::ifstream input;

    void read_next();

public:
    std::shared_ptr<SeriesIndex> p_index;

    SeriesReader(std::string path="/dev/stdin", bool strict=true);
    ~SeriesReader();

    bool eof();
    Series next();
};

};
