#pragma once

#include <string>
#include <vector>
#include <cstdint>

namespace matrixdb {

std::string compress_string(const std::string& s);
std::string decompress_string(const std::string& s);

std::vector<uint8_t> compress(std::vector<uint8_t>&);
std::vector<uint8_t> decompress(std::vector<uint8_t>&);

std::vector<uint8_t> compress_vector(std::vector<double>&);
std::vector<double> decompress_vector(std::vector<uint8_t>&);

};
