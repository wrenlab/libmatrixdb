#pragma once
#include <string>

namespace matrixdb {
namespace base64 {

std::string encode(std::string const&);
std::string decode(std::string const&);

};
};
