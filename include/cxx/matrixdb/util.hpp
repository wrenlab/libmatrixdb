#pragma once

#include <sys/stat.h>

#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include <spdlog/spdlog.h>

namespace matrixdb {

std::shared_ptr<spdlog::logger> LOG();

bool file_exists(const std::string& path);

std::vector<std::string> split(const std::string &text, char sep='\t');

class ProgressBar {
private:
    std::ostream* output;
    size_t current = 0;
    size_t maximum;
    size_t width = 40;
    void print();

public:
    ProgressBar(std::ostream*, size_t);
    void update(size_t);
    void finish();
};

template <typename T>
std::vector<std::vector<T> >
transpose_vv(std::vector<std::vector<T> >& X) {
    size_t nc = -1;
    for (auto x : X) {
        if (nc != -1) {
            assert(x.size() == nc);
        }
        nc = x.size();
    }
    size_t nr = X.size();

    std::vector<std::vector<T> > Xt;
    for (int j=0; j<nc; j++) {
        Xt.push_back(std::vector<T>(nr));
        std::vector<T>& v = Xt[j];
        for (int i=0; i<nr; i++) {
            v[i] = X[i][j];
        }
    }
    return Xt;
};

};
