#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <vector>

#include <sqlite3.h>

namespace matrixdb {

class PreparedStatement {
    bool is_open;
    sqlite3_stmt* ps;
    bool _has_next = false;
public:
    PreparedStatement(sqlite3*, std::string);
    ~PreparedStatement() {
        commit();
    }

    void close();
    void commit();

    void bind_text(size_t, const std::string&);
    void bind_integer(size_t, int64_t);
    void bind_blob(size_t, const void*, size_t);

    int64_t get_integer(size_t);
    std::string get_text(size_t);
    std::vector<uint8_t> get_blob(size_t);

    void step(bool reset=false);
    bool has_next();
};

class SQLiteDB {
protected:
    std::string path;
    bool read_only;
    bool is_open;
    sqlite3* cx;
    
public:
    SQLiteDB(const std::string path, bool read_only=true);

    ~SQLiteDB() {
        close();
    }

    void close();
    void execute(const std::string);
    
    PreparedStatement prepare_statement(const std::string sql) {
        return PreparedStatement(cx, sql);
    }
};

};
