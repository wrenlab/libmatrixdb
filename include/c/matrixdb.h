#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef void matrixdb_t;
typedef void matrixdb_vector_t;
typedef const void matrixdb_index_t;

typedef struct {
  void* data;
  size_t length;
} matrixdb_blob_t;

/* String */
void matrixdb_blob_free(matrixdb_blob_t*);
/*
size_t matrixdb_string_size(matrixdb_string_t*);
void matrixdb_string_data(matrixdb_string_t*, char*);
*/

/* DB functions */

matrixdb_t* matrixdb_open(const char* path, bool read_only=true);
void matrixdb_import_data(matrixdb_t*, const char*, bool);
void matrixdb_close(matrixdb_t*);

/* Vector functions */

matrixdb_vector_t* matrixdb_get_column(matrixdb_t*, const char* key);
matrixdb_vector_t* matrixdb_get_row(matrixdb_t*, const char* key);
const char* matrixdb_vector_key(matrixdb_vector_t*);
size_t matrixdb_vector_size(matrixdb_vector_t*);
double* matrixdb_vector_data(matrixdb_vector_t*);
void matrixdb_vector_delete(matrixdb_vector_t*);

/* Index functions */

matrixdb_index_t* matrixdb_get_row_index(matrixdb_t*);
matrixdb_index_t* matrixdb_get_column_index(matrixdb_t*);
size_t matrixdb_index_size(matrixdb_index_t* ix);
void matrixdb_index_element(matrixdb_index_t* ix, size_t i, char* result);

/* Metadata functions */

void matrixdb_metadata_set(matrixdb_t*, const char*, matrixdb_blob_t*);
matrixdb_blob_t* matrixdb_metadata_get(matrixdb_t*, const char*);

/* Operations */

/*
matrixdb_vector* matrixdb_collapse(matrixdb* db, 
        char** keys, 
        size_t n, 
        size_t,
        const char*);
*/

#ifdef __cplusplus
}
#endif
