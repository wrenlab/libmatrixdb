#include <matrixdb/util.hpp>

namespace matrixdb {

std::shared_ptr<spdlog::logger> _LOG = NULL;

std::shared_ptr<spdlog::logger>
LOG() {
    if (_LOG == NULL)
        _LOG = spdlog::stderr_color_mt("libwrenlab");
    return _LOG;
}

bool 
file_exists(const std::string& path) {
    struct stat buffer;
    return (stat (path.c_str(), &buffer) == 0); 
}

std::vector<std::string> 
split(const std::string &text, char sep) {
    std::vector<std::string> tokens;
    int start = 0, end = 0;
    while ((end = text.find(sep, start)) != std::string::npos) {
        tokens.push_back(text.substr(start, end - start));
        start = end + 1;
    }
    tokens.push_back(text.substr(start));
    return tokens;
}

ProgressBar::ProgressBar(std::ostream* output, size_t maximum) {
    this->output = output;
    this->maximum = maximum;
    this->current = current;
    print();
}

void
ProgressBar::print() {
    (*output) << "[";
    double progress = ((1.0 * current) / maximum);
    int position = width * progress;
    for (int i=0; i<width; ++i) {
        if (i < position)
            (*output) << "=";
        else if (i == position)
            (*output) << ">";
        else
            (*output) << " ";
    }
    (*output) << "] " 
        << int(progress * 100.0) 
        << "% (" << current << "/" << maximum << ")\r" ;
    (*output).flush();
}

void
ProgressBar::update(size_t new_value) {
    this->current = new_value;
    print();
}

void 
ProgressBar::finish() {
    this->current = maximum;
    print();
    (*output) << std::endl;
}

};
