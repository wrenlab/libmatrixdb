#include <iostream>

#include <armadillo>

#include <matrixdb/io.hpp>
#include <matrixdb/util.hpp>

namespace matrixdb {

SeriesReader::SeriesReader(std::string path, bool strict) {
    this->strict = strict;

    input.open(path.c_str());
    std::string header;
    getline(input, header);
    std::vector<std::string> labels = split(header);
    labels.erase(labels.begin());

    p_index = std::make_shared<SeriesIndex>();
    p_index->initialize(labels);

    read_next();
}

void
SeriesReader::read_next() {
    std::string line;
    if (!getline(input, line)) {
        _eof = true;
        return;
    }
    std::vector<std::string> fields = split(line);

    std::vector<double> elements;
    for (int i=1; i<fields.size(); i++) {
        elements.push_back(atof(fields[i].c_str()));
    }
    if (!strict) {
        if (elements.size() != p_index->labels.size()) {
            std::cerr << "Failed reading line. Skipping." << std::endl;
            read_next();
            return;
        }
    } 
    Series next(fields[0], p_index, arma::vec(elements));
    current = next;
}

Series
SeriesReader::next() {
    assert(!eof());
    Series o = current;
    read_next();
    return o;
}

bool
SeriesReader::eof() {
    return _eof;
}

SeriesReader::~SeriesReader() {}

};
