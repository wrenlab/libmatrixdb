#include <cassert>
#include <cstring>

#include <zlib.h>

#include <matrixdb/compression.hpp>

namespace matrixdb {

const size_t BUFFER_SIZE = 128 * 1024;

std::vector<uint8_t>
compress(std::vector<uint8_t>& input) {
  std::vector<uint8_t> o;

  uint8_t buffer[BUFFER_SIZE];

  z_stream z;
  z.zalloc = 0;
  z.zfree = 0;
  z.next_in = &input[0];
  z.avail_in = input.size();
  z.next_out = buffer;
  z.avail_out = BUFFER_SIZE;

  deflateInit(&z, Z_BEST_SPEED);

  while (z.avail_in != 0)
  {
    int rc = deflate(&z, Z_NO_FLUSH);
    assert(rc == Z_OK);
    if (z.avail_out == 0)
    {
      o.insert(o.end(), buffer, buffer + BUFFER_SIZE);
      z.next_out = buffer;
      z.avail_out = BUFFER_SIZE;
    }
  }

  int rc = Z_OK;
  while (rc == Z_OK)
  {
    if (z.avail_out == 0)
    {
      o.insert(o.end(), buffer, buffer + BUFFER_SIZE);
      z.next_out = buffer;
      z.avail_out = BUFFER_SIZE;
    }
    rc = deflate(&z, Z_FINISH);
  }

  assert(rc == Z_STREAM_END);
  o.insert(o.end(), buffer, buffer + BUFFER_SIZE - z.avail_out);
  deflateEnd(&z);

  return o;
}

std::vector<uint8_t>
decompress(std::vector<uint8_t>& input) {
	uint8_t buffer[BUFFER_SIZE];
	std::vector<uint8_t> o;

	z_stream z;
	z.zalloc = Z_NULL;
	z.zfree = Z_NULL;
	z.opaque = Z_NULL;
	z.avail_in = input.size();
	z.next_in = (unsigned char*) &input[0];
	z.avail_out = BUFFER_SIZE;
	z.next_out = (unsigned char*) buffer;

	assert(inflateInit(&z) == Z_OK);

	while (z.avail_in != 0)
  {
    int rc = inflate(&z, Z_NO_FLUSH);
		if (rc == Z_STREAM_END) {
			rc = inflate(&z, Z_FINISH);
  		o.insert(o.end(), buffer, buffer + BUFFER_SIZE - z.avail_out);
			break;
		} else if (rc == Z_OK) {
			if (z.avail_out == 0)
			{
				o.insert(o.end(), buffer, buffer + BUFFER_SIZE);
				z.next_out = buffer;
				z.avail_out = BUFFER_SIZE;
			}
		} else {
			assert(false);
		}
  }
  inflateEnd(&z);

	return o;
}

std::vector<uint8_t>
compress_vector(std::vector<double>& v) {
  size_t n = v.size() * sizeof(double);
  std::vector<uint8_t> input(n);
  memcpy(&input[0], &v[0], n);
	return compress(input);
}

std::vector<double>
decompress_vector(std::vector<uint8_t>& input) {
  std::vector<uint8_t> o = decompress(input);
	assert(o.size() % sizeof(double) == 0);
	std::vector<double> ov(o.size() / sizeof(double));
	memcpy(&ov[0], &o[0], o.size());
  return ov;
}

std::string
compress_string(const std::string& s) {
    std::string o;
    size_t ratio = 2;

    z_stream z;

    /* FIXME: should use a buffer instead of this inefficient method... */
    do {
        z.zalloc = Z_NULL;
        z.zfree = Z_NULL;
        z.opaque = Z_NULL;

        size_t size = s.length() * 2;
        o.resize(size);

        z.avail_in = s.length();
        z.next_in = (unsigned char*) &s[0];
        z.avail_out = size;
        z.next_out = (unsigned char*) &o[0]; 

        assert (deflateInit(&z, Z_BEST_SPEED) == Z_OK);
        deflate(&z, Z_FINISH);
        deflateEnd(&z);

        //ratio *= round(s.length() / z.avail_in) + 1;
        ratio *= 2;
        o.resize(size - z.avail_out);
    } while (z.avail_in != 0);

    return o;
}

std::string
decompress_string(const std::string& s) {
    size_t ratio = 2;

    std::string o;
    size_t avail_in;

    /* FIXME: should use a buffer instead of this inefficient method... */
    do {
        z_stream z;
        z.zalloc = Z_NULL;
        z.zfree = Z_NULL;
        z.opaque = Z_NULL;

        size_t size = s.length() * ratio;
        o.resize(size);

        z.avail_in = s.size();
        z.next_in = (unsigned char*) &s[0];
        z.avail_out = o.size();
        z.next_out = (unsigned char*) &o[0];
         
        assert(inflateInit(&z) == Z_OK);
        inflate(&z, Z_NO_FLUSH);
        inflateEnd(&z);

        //std::cerr << s.length() << "\t" << z.avail_in << std::endl;
        //ratio *= round(s.length() / z.avail_in) + 1;
        //std::cerr << "factor: " << round(s.length() / z.avail_in) + 1 << std::endl;
        ratio *= 2;
        o.resize(size - z.avail_out);	
        avail_in = z.avail_in;
    } while (avail_in != 0);

    return o;
};

};
