#include <string>
#include <iterator>
#include <cstring>

#include <armadillo>

#include <matrixdb.hpp>
#include <matrixdb.h>

using namespace matrixdb;

/* C API */

typedef const std::vector<std::string> Index;

extern "C" {

/* Utility */

void 
matrixdb_blob_free(matrixdb_blob_t* s) {
  free(s);
}

/* General */ 

matrixdb_t* 
matrixdb_open(const char* _path, bool read_only) {
    std::string path(_path);
    MatrixDB* db = new MatrixDB(path, read_only);
    return reinterpret_cast<matrixdb_t*>(db);
}

void
matrixdb_close(matrixdb_t* db) {
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    _db->close();
}

void 
matrixdb_import_data(matrixdb_t* db, const char* input_path, bool transpose) {
    MatrixDB* X = reinterpret_cast<MatrixDB*>(db);
    const char* tmpdir = std::getenv("TMPDIR");
    if (tmpdir == NULL)
      tmpdir = "/tmp";
    SeriesReader reader(input_path, false);
    X->initialize(reader, tmpdir, transpose);
}

/* row/column functions */

matrixdb_vector_t* matrixdb_get_column(matrixdb_t* db, const char* _key) {
    std::string key(_key);
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    Vector* column = _db->column(key);
    return reinterpret_cast<matrixdb_vector_t*>(column);
}

matrixdb_vector_t*
matrixdb_get_row(matrixdb_t* db, const char* _key) {
    std::string key(_key);
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    Vector* row = _db->row(key);
    return reinterpret_cast<matrixdb_vector_t*>(row);
}

const char*
matrixdb_vector_key(matrixdb_vector_t* row) {
    Vector* r = reinterpret_cast<Vector*>(row);
    return &r->key[0];
}

size_t
matrixdb_vector_size(matrixdb_vector_t* row) {
    Vector* r = reinterpret_cast<Vector*>(row);
    return r->data.size();
}

double*
matrixdb_vector_data(matrixdb_vector_t* row) {
    Vector* r = reinterpret_cast<Vector*>(row);
    return &r->data[0];
}

void
matrixdb_vector_delete(matrixdb_vector_t* row) {
    Vector* r = reinterpret_cast<Vector*>(row);
    delete r;
}

/* Index functions */

matrixdb_index_t*
matrixdb_get_row_index(matrixdb_t* db) {
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    return reinterpret_cast<matrixdb_index_t*>(&_db->index());
}

matrixdb_index_t*
matrixdb_get_column_index(matrixdb_t* db) {
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    return reinterpret_cast<matrixdb_index_t*>(&_db->columns());
}

size_t
matrixdb_index_size(matrixdb_index_t* ix) {
    Index* _ix = reinterpret_cast<Index*>(ix);
    return _ix->size();
}

void
matrixdb_index_element(matrixdb_index_t* ix, size_t i, char* result) {
    Index* _ix = reinterpret_cast<Index*>(ix);
    const std::string& s = (*_ix)[i];
    strncpy(result, s.c_str(), s.size());
    result[s.size()] = '\0';
}

/* Metadata */

void 
matrixdb_metadata_set(matrixdb_t* db, const char* key, matrixdb_blob_t* value) {
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    std::string v((const char*) value->data, value->length);
    _db->metadata.set(key, v);
}

matrixdb_blob_t*
matrixdb_metadata_get(matrixdb_t* db, const char* key) {
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    std::string s = _db->metadata.get(key);
    matrixdb_blob_t* o = (matrixdb_blob_t*) malloc(sizeof(matrixdb_blob_t));
    o->length = s.size();
    o->data = malloc(s.size());
    memcpy(o->data, s.c_str(), o->length);
    return o;
}

/* Operations */

/*
matrixdb_vector*
matrixdb_collapse(matrixdb* db, char** keys, size_t n, 
        size_t axis_,
        const char* collapse_method) {

    MatrixDB::AXIS axis;
    MatrixDB::REDUCTION2D method;
    std::string m_str(collapse_method);

    if (m_str == "sum") {
        method = MatrixDB::REDUCTION2D::SUM;
    } else if (m_str == "mean") {
        method = MatrixDB::REDUCTION2D::MEAN;
    } else if (m_str == "pc0") {
        method = MatrixDB::REDUCTION2D::PCA;
    } else {
        std::string msg = "Collapse method '" + m_str + "' not found.";
        throw std::runtime_error(msg);
    }

    if (axis_ == 0) {
        axis = MatrixDB::AXIS::ROW;
    } else if (axis_ == 1) {
        axis = MatrixDB::AXIS::COLUMN;
    } else {
        throw std::runtime_error("Collapse axis must be 0 or 1.");
    }

    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    std::vector<std::string> ix;
    for (size_t i=0; i<n; i++) {
        ix.push_back(keys[i]);
    }
    arma::vec v = _db->collapse(
            ix,
            axis,
            method);
    Vector* o = new Vector("", arma::conv_to<std::vector<double> >::from(v));
    return o;
}
*/

}


