#include <matrixdb/sqlite.hpp>

namespace matrixdb {

SQLiteDB::SQLiteDB(const std::string path, bool read_only) {
    this->path = path;
    int flags;
    this->read_only = read_only;
    if (read_only) {
        flags = SQLITE_OPEN_READONLY;
    } else {
        flags = SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
    }
    sqlite3_open_v2(path.c_str(), &cx, flags, NULL);
    this->is_open = true;
}

void
SQLiteDB::close() {
    if (is_open) {
        sqlite3_close_v2(cx);
        this->is_open = false;
    }
}

void
SQLiteDB::execute(const std::string sql) {
    sqlite3_exec(cx, sql.c_str(), NULL, NULL, NULL);
}

PreparedStatement::PreparedStatement(sqlite3* cx, std::string sql) {
    int rc = sqlite3_prepare_v2(cx, sql.c_str(), sql.size(), &ps, NULL);
    is_open = true;
}

void 
PreparedStatement::close() { 
    commit(); 
}

void
PreparedStatement::commit() {
    if (is_open) {
        sqlite3_finalize(ps);
        this->is_open = false;
    }
}

void 
PreparedStatement::bind_text(size_t index, const std::string& value) {
    sqlite3_bind_text64(ps, index+1, value.c_str(), value.size(), 
            SQLITE_STATIC, SQLITE_UTF8);
}

void
PreparedStatement::bind_integer(size_t index, int64_t value) {
    sqlite3_bind_int64(ps, index+1, value);
}

void
PreparedStatement::bind_blob(size_t index, const void* value, size_t size) {
    sqlite3_bind_blob64(ps, index+1, (const void*) value,
            size, SQLITE_STATIC);
}

int64_t 
PreparedStatement::get_integer(size_t index) {
    return sqlite3_column_int64(ps, index);
}

std::string
PreparedStatement::get_text(size_t index) {
    const char* o = (const char*) sqlite3_column_text(ps, index);
    return std::string(o);
}

std::vector<uint8_t>
PreparedStatement::get_blob(size_t index) {
    uint8_t* data = (uint8_t*) sqlite3_column_blob(ps, index);
    size_t length = sqlite3_column_bytes(ps, index);
    std::vector<uint8_t> o;
    o.assign(data, data+length);
    return o;
}

void
PreparedStatement::step(bool reset) {
    int rc = sqlite3_step(ps);
    if (rc == SQLITE_ROW) {
        _has_next = true;
    } else {
        _has_next = false;
    }
    if (reset)
        sqlite3_reset(ps);
}

bool
PreparedStatement::has_next() {
    return _has_next;
}

};
