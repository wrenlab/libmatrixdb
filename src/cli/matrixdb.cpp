#include <string>
#include <iostream>
#include <set>
#include <map>
#include <vector>

#include <matrixdb.hpp>
#include <matrixdb.h>

using namespace matrixdb;

void
print_usage() {
    std::cerr 
        << "USAGE: matrixdb <command> [arguments]\n\n"
        << "Manipulate a MatrixDB database.\n";
}

void dump(int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    MatrixDB X(path, true);
    X.dump(std::cout);
}

void 
load(int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    char c;
    bool transpose = false;
    while ((c = getopt(argc, argv, "t")) != -1) {
        switch (c) {
            case 't':
                transpose = true;
                break;
        }
    }

    std::string path(argv[optind]);
    MatrixDB X(path, false);
    SeriesReader reader("/dev/stdin", false);
    X.initialize(reader, "/data/tmp", transpose);
    X.close();
}

void 
row(int argc, char* argv[]) {
    if (argc < 3) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    std::string key(argv[2]);

    MatrixDB X(path, true);
    Vector* row = X.row(key);

    if (row == NULL) {
        std::cerr << "Row not found: '" << key << "'\n";
        exit(1);
    }

    std::vector<std::string> columns = X.columns();
    for (int i=0; i<columns.size(); i++) {
        std::cout << columns[i] << "\t" << row->data[i] << std::endl;
    }
}

void 
column(int argc, char* argv[]) {
    if (argc < 3) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    std::string key(argv[2]);

    MatrixDB X(path, true);
    Vector* column = X.column(key);

    if (row == NULL) {
        std::cerr << "Row not found: '" << key << "'\n";
        exit(1);
    }

    std::vector<std::string> index = X.index();
    for (int i=0; i<index.size(); i++) {
        std::cout << index[i] << "\t" << column->data[i] << std::endl;
    }
}

void index(int argc, char* argv[]) {
    std::string path(argv[1]);
    MatrixDB X(path, true);
    std::vector<std::string> index = X.index();
    for (auto ix : index) {
        std::cout << ix << std::endl;
    }
}

void columns(int argc, char* argv[]) {
    std::string path(argv[1]);
    MatrixDB X(path, true);
    std::vector<std::string> columns = X.columns();
    for (auto ix : columns) {
        std::cout << ix << std::endl;
    }
}

void
stats (int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    MatrixDB X(path, true);
    std::cout << "Rows: " << X.index().size() << std::endl;
    std::cout << "Columns: " << X.columns().size() << std::endl;
}

void
verify(int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    MatrixDB X(path, true);
    for (const std::string& ix : X.index()) {
        auto row = X.row(ix);
    }
}

void
test(int argc, char* argv[]) {
  std::string path(argv[1]);
  MatrixDB X (path, false);
  X.metadata.set("abc", "def");
  /*
  matrixdb_blob_t* s = matrixdb_metadata_get((void*) &X, "abc");
  std::cerr << s->length << std::endl;
  */
}

/*
int
collapse(int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    MatrixDB::AXIS axis = MatrixDB::AXIS::ROW;
    MatrixDB::REDUCTION2D reduction = MatrixDB::REDUCTION2D::SUM;
    char c;
    while ((c = getopt(argc, argv, "cm:")) != -1) {
        switch (c) {
            case 'c':
                axis = MatrixDB::AXIS::COLUMN;
                LOG()->info("Collapsing along columns.");
                break;
            case 'm':
                std::string method(optarg);
                if (method == "sum") {
                    reduction = MatrixDB::REDUCTION2D::SUM;
                } else if (method == "mean") {
                    reduction = MatrixDB::REDUCTION2D::MEAN;
                } else if (method == "max_mean") {
                    reduction = MatrixDB::REDUCTION2D::MAX_MEAN;
                } else {
                    std::string msg = "Collapse method must be one of: 'sum', 'mean', 'max_mean'. Got: '" + method + "'.";
                    throw std::runtime_error(msg);
                }
                break;
        }
    }

    if (argc != optind + 1) {
        print_usage();
        exit(1);
    }
    std::string path(argv[optind]);
    MatrixDB X(path, true);

    std::vector<std::string> ix, ix_t;
    if (axis == MatrixDB::AXIS::ROW) {
        ix = X.index();
        ix_t = X.columns();
    } else {
        ix = X.columns();
        ix_t = X.index();
    }

    std::set<std::string> ix_s(ix.begin(), ix.end());
    std::string line;
    std::map<std::string, std::set<std::string> > M;

    while (getline(std::cin, line)) {
        std::vector<std::string> fields = wrenlab::split(line, '\t');
        if (fields.size() != 2)
            continue;
        std::string from = fields[0];
        std::string to = fields[1];
        if (ix_s.find(from) != ix_s.end()) {
            M[to].insert(from);
        }
    }

    LOG()->info("Loaded {0} pairwise mappings.", M.size());

    for (auto k : ix_t) {
        std::cout << "\t" << k;
    }
    std::cout << std::endl;

    for (auto kv : M) {
        std::cout << kv.first;
        std::vector<std::string> keys(kv.second.begin(), kv.second.end());
        arma::vec v = X.collapse(keys, axis, reduction);
        for (size_t i=0; i<v.size(); i++) {
            std::cout << "\t" << v(i);
        }
        std::cout << std::endl;
    }
}

int impute(int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    MatrixDB XDB(path, true);
    arma::mat X = XDB.data();

    arma::mat Xi = wrenlab::statistics::impute::mean(X);
    for (size_t j=0; j<XDB.ncol(); j++) {
        std::cout << "\t" << XDB.columns()[j];
    }
    std::cout << std::endl;
    for (size_t i=0; i<XDB.nrow(); i++) {
        std::cout << XDB.index()[i];
        for (size_t j=0; j<XDB.ncol(); j++) {
            std::cout << "\t" << Xi(i,j);
        }
        std::cout << std::endl;
    }
}
*/

int main(int argc, char* argv[]) {
    if (argc == 1) {
        print_usage();
        exit(1);
    }

    std::string cmd(argv[1]);

    if (cmd == "row") {
        row(argc - 1, argv + 1);
    } else if (cmd == "column") {
        column(argc - 1, argv + 1);
    } else if (cmd == "index") {
        index(argc - 1, argv + 1);
    } else if (cmd == "columns") {
        columns(argc - 1, argv + 1);
    } else if (cmd == "dump") {
        dump(argc - 1, argv + 1);
    } else if (cmd == "load") {
        load(argc - 1, argv + 1);
    } else if (cmd == "stats") {
        stats(argc - 1, argv + 1);
    } else if (cmd == "verify") {
        verify(argc - 1, argv + 1);
    } else if (cmd == "test") {
      test(argc - 1, argv + 1);
    //} else if (cmd == "collapse") {
    //    collapse(argc - 1, argv + 1);
    //} else if (cmd == "impute") {
    //    impute(argc - 1, argv + 1);
    } else {
        print_usage();
        exit(1);
    }
}
